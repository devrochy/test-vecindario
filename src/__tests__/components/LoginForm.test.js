import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import LoginForm from '../../components/Auth/LoginForm/LoginForm';
import { LOGIN_GQL } from '../../gql/user';
import { shallow } from 'enzyme';
import userEvent from '@testing-library/user-event'

describe('Pruebas en <LoginForm />', () => {

    
    const data  = { email: "devrochy@gmail.com", password:"123456" };
    
    const mocks = [
        {
            request: {
                query: LOGIN_GQL,
                variables: { email: "devrochy@gmail.com", password:"123456" },
            },
            result: {
                login: {
                    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNWRkZDljNGQ3ZTJmN2Y5YmI4M2IwOCIsIm5hbWUiOiJSb2JpbnNvbiBBZ3VpbGFyIiwiZW1haWwiOiJkZXZyb2NoeUBnbWFpbC5jb20iLCJ1c2VybmFtZSI6ImRldnJvY2h5IiwiaWF0IjoxNjMzOTU3MzEwLCJleHAiOjE2MzQwNDM3MTB9.9Rlbd_Vc0Ntk5ySe_cirnCXhgxQpr_1fm3S7OnR5UqQ"
                },
            },
        },
    ];

    const wrapper = render( 
        <MockedProvider mocks={mocks} addTypename={false}>
            <LoginForm />
        </MockedProvider>
    );

    test("Debe mostrar el componente correctamente", () => {
        expect(wrapper).toMatchSnapshot();
    });

    test("Debe mostrar el componente correctamente", () => {
        // fireEvent.change(wrapper("email"), { target: { value: 'devrochy@gmail.com' } }); // invoke handleChange
        // fireEvent.change(wrapper("password"), { target: { value: '123456' } }); // invoke handleChange
        // expect(wrapper.find('input[name="email"]').value).to.equal('devrochy@gmail.com')
        // expect(wrapper.find('input[name="password"]').value).to.equal('123456')

        // wrapper.find("LoginForm").dive().find("login-form").simulate("submit");
        wrapper.getByTestId("email").value = "devrochy@gmail.com";
        wrapper.getByTestId("password").value = "123456";

        
    });
    
});
