import React, { useState } from 'react';
import { Form, Button } from "semantic-ui-react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useMutation } from "@apollo/client";
import { LOGIN_GQL } from "../../../gql/user";
import { setToken, decode } from "../../../utils/token";
import useAuth from '../../../hooks/useAuth';
import "./LoginForm.scss";

export default function LoginForm() {
    const [error, setError] = useState("");
    const [login] = useMutation(LOGIN_GQL);

    const { setUser } = useAuth();

    const formik = useFormik({
        initialValues: initialValues(),
        validationSchema: Yup.object({
            email: Yup.string().email().required(),
            password: Yup.string().required(),
        }),
        onSubmit: async (formData) => {
            console.log('Submit')
            setError("");
            try {
                const { data } = await login({
                    variables: {
                        input: formData
                    }
                });
                const { token } = data.login;
                setToken(token);
                setUser(decode(token));
            } catch (error) {
                setError(error.message)
            }
        }
    });

    return (
        <Form className="login-form" onSubmit={formik.handleSubmit}>
            <h2>Entra para ver que estan haciendo tus amigos</h2>
            <Form.Input 
                type="text" 
                placeholder="Correo electronico" 
                name="email" 
                data-testid="email"
                value={formik.values.email}
                onChange={formik.handleChange} 
                error={formik.errors.email && true}
            />
            <Form.Input 
                type="password" 
                placeholder="Contraseña" 
                name="password" 
                data-testid="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                error={formik.errors.password && true}
            />

            <Button type="submit" className="btn-submit">
                Iniciar Sesión
            </Button>
            {error && <p className="submit-error">{error}</p>}
        </Form>
    )
}

function initialValues() {
    return {
        email: "",
        password: "",
    }
}

