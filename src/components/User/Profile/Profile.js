import React, { useState } from 'react';
import { Grid, Image } from "semantic-ui-react";
import { useQuery } from "@apollo/client";
import { GET_USER } from "../../../gql/user";
import useAuth from '../../../hooks/useAuth';
import ModalBasic from "../../Modal/ModalBasic"
import ImageNoFound from "../../../assets/png/av-1.png";
import UserNotFound from '../../UserNotFound';
import HeaderProfile from './HeaderProfile';
import SettingsForm from '../SettingsForm';
import Followers from "./Followers";
import "./Profile.scss";
import PropTypes from 'prop-types';

export default function Profile(props) {
    const { username, totalPublications } = props;
    const [showModal, setShowModal] = useState(false);
    const [titleModal, setTitleModal] = useState("");
    const [childrenModal, setChildrenModal] = useState(null);
    const { auth } = useAuth();
    const { data, loading, error, refetch } = useQuery(GET_USER, {
        variables: { username }
    }); 

    if(loading) return null;
    if(error) return <UserNotFound />;
    const { getUser } = data;

    const handlerModal = (type) => {
        switch (type) {
            case "settings":
                //setTitleModal("Ajustes");
                setChildrenModal(<SettingsForm 
                    setShowModal={setShowModal} 
                    setTitleModal={setTitleModal}
                    setChildrenModal={setChildrenModal}
                    getUser={getUser}
                    refetch={refetch}/>);
                setShowModal(true);
                break;
            default:
                break;
        }
    }

    return (
        <>
            <Grid className="profile">
                <Grid.Column width={5} className="profile__left">
                    <Image src={getUser.avatar ? getUser.avatar : ImageNoFound} avatar/>
                </Grid.Column>
                <Grid.Column width={11} className="profile__right">
                    <HeaderProfile getUser={getUser} auth={auth} handlerModal={handlerModal}/>
                    <Followers username={username} totalPublications={totalPublications} />
                    <div className="">
                        <p className="name">{getUser.name}</p>
                        {getUser.siteWeb && (
                            <a href={getUser.siteWeb} className="siteWeb" target="_blank">
                                {getUser.siteWeb}
                            </a>
                        )}
                        {getUser.description && (
                            <p className="description">{getUser.description}</p>
                        )}
                    </div>
                </Grid.Column>
            </Grid>
            <ModalBasic show={showModal} setShow={setShowModal} title={titleModal}>
                {childrenModal}
            </ModalBasic>
        </>
    )
}

Profile.propTypes = {
    username: PropTypes.string.isRequired,
    totalPublications: PropTypes.string.isRequired
}
