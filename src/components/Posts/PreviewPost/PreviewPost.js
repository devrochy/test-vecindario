import React, { useState } from 'react';
import { Image } from "semantic-ui-react";
import ModalPreview from '../../Modal/ModalPreview';
import "./PreviewPost.scss";

export default function PreviewPost(props) {
    const { publication } = props;
    const [showModal, setShowModal] = useState(false);

    return (
        <>
            <div className="preview-post" onClick={() => setShowModal(true)}>
                <Image className="preview-post__image" src={publication.file}/>
            </div>
            <ModalPreview show={showModal} setShow={setShowModal} publication={publication} />
        </>
    )
}
