import React from 'react';
import { Grid } from "semantic-ui-react";
import { map } from 'lodash';
import "./Posts.scss";
import PreviewPost from './PreviewPost';

export default function Posts(props) {
    const { getPublications } = props;

    return (
        <div className="posts">
            <h1>Publicaciones</h1>
            <Grid columns={4}>
                {map(getPublications, (publication, index) => (
                    <Grid.Column key={index}>
                        
                        <PreviewPost  publication={publication} />
                    </Grid.Column>
                ))}

            </Grid>
        </div>
    )
}
