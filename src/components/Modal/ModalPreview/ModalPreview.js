import React from "react";
import { Modal, Grid } from "semantic-ui-react";
import Comments from "./Comments";
import Actions from "./Actions";
import FormComment from "./FormComment";
import "./ModalPreview.scss";

export default function ModalPreview(props) {
  const { show, setShow, publication } = props;

  const onClose = () => setShow(false);

  return (
    <Modal open={show} onClose={onClose} className="modal-preview">
      <Grid>
        <Grid.Column
          className="modal-preview__left"
          width={10}
          style={{ backgroundImage: `url("${publication.file}")` }}
        />
        <Grid.Column className="modal-preview__right" width={6}>
          <Comments publication={publication} />
          <Actions publication={publication} />
          <FormComment publication={publication} />
        </Grid.Column>
      </Grid>
    </Modal>
  );
}