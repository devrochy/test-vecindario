import React from 'react';
import { Link } from "react-router-dom";
import "./UserNotFound.scss";


export default function UserNotFound() {
    return (
        <div className="user-not-found">
            <p>Usuario no encontrado</p>
            <p>
                El enlace que estas consultando no es correcto o el usuario no existe!, por favor verifique su consulta.
            </p>
            <Link to="/">Regresar al home</Link>
        </div>
    )
}
