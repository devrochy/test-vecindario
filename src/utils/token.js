import jwtDecode from "jwt-decode";
import { TOKEN } from "./constants";

export function setToken( token ) {
    localStorage.setItem(TOKEN, token);
}

export function getToken( token ) {
    return localStorage.getItem(TOKEN);
}

export function decode( token ) {
    return jwtDecode( token );
}

export function remove() {
    localStorage.removeItem(TOKEN);
}