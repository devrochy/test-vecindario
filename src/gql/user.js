import { gql } from "@apollo/client";

export const REGISTER = gql`
    mutation register($input: UserInput) {
        register(input: $input) {
            id
            name
            username
            email
            createAt
        }
    }
`

export const LOGIN_GQL = gql`
    mutation login($input: LoginInput) {
        login(input: $input) {
            token
        }
    }
`
export const GET_USER = gql`
    query Query($id: ID, $username: String) {
        getUser(id: $id, username: $username) {
            id
            name
            username
            email
            siteWeb
            description
            avatar
            createAt
            password
        }
    }
`